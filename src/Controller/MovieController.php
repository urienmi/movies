<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Movie;

use App\Data\SearchData;

use App\Form\Movie\SearchForm;
use App\Form\Movie\MovieType;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class MovieController extends AbstractController
{
    
    /**
     * @Route("/movie/", name="home")
     */
    public function home()
    {
        return $this->render('movie/home.html.twig');
    }

    /**
     * @Route("/movie/search", name="search")
     */
    public function search(PaginatorInterface $paginator, Request $request){
        
        $data = new SearchData();
        $form = $this->createForm(SearchForm::class, $data);
        $form->handleRequest($request);

        $result = $this->getDoctrine()->getRepository(Movie::class)->findSearch($data);

        $movies = $paginator->paginate(
            $result,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('movie/search.html.twig', [
            'liste' => $movies,
            'nbMovies' => count($result),
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/movie/add", name="add_movie")
     */
    public function addMovie(Request $request)
    {
        $movie = new Movie();

        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $movie = $form->getData();
            
            dump($movie);
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($movie);
            $manager->flush();

            return $this->render('movie/success.html.twig', array(
                'movie' => $movie
            ));
        }

        return $this->render('movie/add.html.twig', [
            'formMovie' => $form->createView()
        ]);
    }

    /**
     * @Route("/movie/success", name="movie_success")
     */
    public function addSuccess($movie){
        return $this->render('movie/success.html.twig', [
            'newMovie' => $movie
        ]);
    }

    /**
     * @Route("/movie/{id}", name="movie_show")
     * @param $id integer
     * @return Response
     */
    public function getMovieById($id)
    {
        $movie = $this->getDoctrine()
            ->getRepository(Movie::class)
            ->find($id);
        
        if (!$movie){
            throw $this->createNotFoundException('No movie found for id '.$id);
        }

        return $this->render('movie/movieById.html.twig', [
            'movie' => $movie,
        ]);
    }

    

}

?>