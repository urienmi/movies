<?php

namespace App\Data;

class SearchData
{
    /**
     * @var string
     */
    public $q = '';

    /**
     * @var null|integer
     */
    public $maxDate;

    /**
     * @var null|integer
     */
    public $minDate;

    /**
     * @var null|integer
     */
    public $orderBy;
}

?>