<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;


class TwigExtension extends AbstractExtension {

    public function getFilters(){
        return [new TwigFilter('strong', [$this, 'strong'], ['is_safe' => ['html']])];
    }

    public function strong($content){
        return '<div style="font-weight: bold">'.$content.'</div>';
    }
}

?>