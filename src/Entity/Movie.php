<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 * @Orm\Table(name="`movie`")
 * @Vich\Uploadable()
 */


class Movie
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Titre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Casting;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Realisateur;

    /**
     * @ORM\Column(type="integer")
     */
    private $Annee;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="movie_image", fileNameProperty="filename")
     */
    private $Image;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Synopsis;









    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $Id): self
    {
        $this->Id = $Id;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getCasting(): ?string
    {
        return $this->Casting;
    }

    public function setCasting(string $Casting): self
    {
        $this->Casting = $Casting;

        return $this;
    }

    public function getRealisateur(): ?string
    {
        return $this->Realisateur;
    }

    public function setRealisateur(string $Realisateur): self
    {
        $this->Realisateur = $Realisateur;

        return $this;
    }

    public function getAnnee(): ?int
    {
        return $this->Annee;
    }

    public function setAnnee(int $Annee): self
    {
        $this->Annee = $Annee;

        return $this;
    }


    public function getImage(): ?File
    {
        return $this->Image;
    }

    public function setImage(?File $Image): Movie
    {
        $this->Image = $Image;

        return $this;
    }

    public function getSynopsis(): ?string
    {
        return $this->Synopsis;
    }

    public function setSynopsis(?string $Synopsis): self
    {
        $this->Synopsis = $Synopsis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string|null $filename
     */
    public function setFilename(?string $filename): void
    {
        $this->filename = $filename;
    }

}
