<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Movie;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    /**
     * Récupère les films en lien avec une recherche
     * @param SearchData $search
     * @return Movie[]
     */
    public function findSearch(SearchData $search): array
    {
        $query = $this
            ->createQueryBuilder('m')
            ->select('m');

        if (!empty($search->orderBy) || $search->orderBy<=3)
        {
            switch ($search->orderBy){
                case 0: $query = $query->orderBy('m.Annee', 'DESC');
                    break;
                case 1: $query = $query->orderBy('m.Annee', 'ASC');
                    break;
                case 2: $query = $query->orderBy('m.Titre', 'ASC');
                    break;
                case 3: $query = $query->orderBy('m.Titre', 'DESC');
                    break;
            }
        }

        if (!empty($search->q))
        {
            $query = $query
                ->andWhere('m.Titre LIKE :q OR m.Realisateur LIKE :q OR m.Casting LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }

        if (!empty($search->minDate) && !empty($search->maxDate))
        {
            $query = $query
                ->andWhere('m.Annee >= :dateMin AND m.Annee <= :dateMax')
                ->setParameter('dateMin', $search->minDate)
                ->setParameter('dateMax', $search->maxDate);
        }else if (!empty($search->minDate))
        {
            $query = $query
                ->andWhere('m.Annee >= :date')
                ->setParameter('date', $search->minDate);
        }else if (!empty($search->maxDate))
        {
            $query = $query
                ->andWhere('m.Annee <= :date')
                ->setParameter('date', $search->maxDate);
        }

        return $query->getQuery()->getResult();
    }


    public function getNbMovies(): string{
        $entityManager = $this->getEntityManager();
        
        $query = $entityManager->createQueryBuilder()
                                ->select('count(n.id)')
                                ->from('App\Entity\Movie', 'n') ;
        

        return $query->getQuery()->getSingleScalarResult();
    }
}
