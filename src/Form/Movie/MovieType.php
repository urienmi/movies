<?php

namespace App\Form\Movie;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Titre', TextType::class, [
                'label' => 'Titre*',
                'attr' => [
                    'placeholder' => "Les Affranchis",
                ]
            ])
            ->add('Synopsis', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Dans les années 1950, à Brooklyn, le jeune Henry Hill a l\'occasion de réaliser son rêve : devenir gangster lorsqu\'un caïd local l\'intègre à son équipe. C\'est alors qu\'il rencontre James et Tommy, 2 truands d\'une rare brutalité. Lorsqu\'il séduit la ravissante Karen, le jeune mafieux s\'imagine que plus rien ni personne ne pourra jamais lui résister.',
                    'maxlength' => 500
                ]
            ])
            ->add('Casting', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => "Robert De Niro, Joe Pesci",
                ]
            ])
            ->add('Realisateur', TextType::class, [
                'label' => 'Réalisateur*',
                'attr' => [
                    'placeholder' => "Martin Scorsese",
                ]
            ])
            ->add('Annee', IntegerType::class, [
                'label' => 'Année*',
                'attr' => [
                    'placeholder' => "1990",
                    'min' => 0,
                    'max' => 2020,
                ]
            ])
            ->add('Image', FileType::class, [
                'label' => 'Image',
                'required' => false,
            ])
        ;
    }
}
?>