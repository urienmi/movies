<?php

namespace App\Form\Movie;

use App\Data\SearchData;
use App\Entity\Movie;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'p-2 w-100',
                    'placeholder' => 'Mots-clés',
                ]
            ])
            ->add('minDate', IntegerType::class, [
                'label' => "Sortis après",
                'required' => false,
                'attr' => [
                    'min' => 0,
                    'max' => 2020,
                    'style' => 'width: 50%',
                    'class' => 'mx-3 py-1',
                    'placeholder' => 'Date minimale'
                ]
            ])
            ->add('maxDate', IntegerType::class, [

                'label' => "Sortis avant",
                'required' => false,
                'attr' => [
                    'min' => 0,
                    'max' => 2020,
                    'style' => 'width: 50%',
                    'class' => 'mx-3 py-1',
                    'placeholder' => 'Date maximale'
                ]
            ])
            ->add('orderBy', ChoiceType::class, [
                'placeholder' => false,
                'label' => 'Trier ',
                'required' => false,
                'attr' => [
                    'class' => 'mx-3 py-1',
                ],
                'choices' => [
                    'Du + au - récent' => 0,
                    'Du - au + récent' => 1,
                    'Par ordre alphabétique' => 2,
                    'Par odre désalphabétique' => 3
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}

?>